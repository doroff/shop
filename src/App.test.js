import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

test('renders add to cart buttons', () => {
  render(<App />);
  const addToCartButtons = screen.queryAllByText(/Add to cart/i);
  expect(addToCartButtons).toHaveLength(10);
});


test('interacts with modal windows', () => {
  const { getByText, queryByText } = render(<App />);

  expect(queryByText(/Do you want to delete this file?/i)).toBeNull();

  fireEvent.click(getByText('🛒'));

  expect(getByText(/Do you want to delete this file?/i)).toBeInTheDocument();

  fireEvent.click(getByText('Cancel'));

  expect(queryByText(/Do you want to delete this file?/i)).toBeNull();

  fireEvent.click(getByText('❤️'));

  expect(getByText(/Do you want to install this file?/i)).toBeInTheDocument();

  fireEvent.click(getByText('OK'));

  expect(queryByText(/Do you want to install this file?/i)).toBeNull();
});

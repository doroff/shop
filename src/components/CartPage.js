import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ProductCard from './ProductCard';
import Modal from './Modal';
import CheckoutForm from './CheckoutForm';

const CartPage = ({ cartItems, removeFromCart }) => {
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [itemToRemove, setItemToRemove] = useState(null);
  const [isFormVisible, setIsFormVisible] = useState(false);
  const [checkoutInfo, setCheckoutInfo] = useState({
    firstName: '',
    lastName: '',
    age: '',
    address: '',
    phone: '',
  });

  const openDeleteModal = (item) => {
    setItemToRemove(item);
    setShowDeleteModal(true);
  };

  const closeDeleteModal = () => {
    setShowDeleteModal(false);
    setItemToRemove(null);
  };

  const handleRemoveFromCart = () => {
    if (itemToRemove) {
      removeFromCart(itemToRemove.id);
      closeDeleteModal();
    }
  };

  const toggleForm = () => {
    setIsFormVisible(!isFormVisible);
  };

  const handleCheckout = () => {
    console.log('Purchased Items:', cartItems);
    console.log('Checkout Info:', checkoutInfo);

    localStorage.removeItem('cartItems');
    localStorage.removeItem('checkoutInfo');

    removeFromCart(null);

    setCheckoutInfo({
      firstName: '',
      lastName: '',
      age: '',
      address: '',
      phone: '',
    });

    setIsFormVisible(false);
  };

  return (
    <div className="cart-page">
      <h2>Cart</h2>
      <div className="product-list">
        {cartItems.map((item) => (
          <div className="cart-product-card" key={item.id}>
            <ProductCard
              product={item}
              isSelected={false} 
            />
            <button className="remove-button" onClick={() => openDeleteModal(item)}>❌</button>
          </div>
        ))}
      </div>
      <button className="open-form-button" onClick={toggleForm}>
        Open Checkout Form
      </button>
      {showDeleteModal && (
        <Modal
          header="Confirm Removal"
          closeButton={true}
          text={`Are you sure you want to remove ${itemToRemove.name} from your cart?`}
          actions={
            <div>
              <button className="modal-button" onClick={handleRemoveFromCart}>OK</button>
              <button className="modal-button" onClick={closeDeleteModal}>Cancel</button>
            </div>
          }
          onClose={closeDeleteModal}
        />
      )}
      {isFormVisible && (
        <div className="checkout-form-container">
          <CheckoutForm />
          <button className="checkout-button" onClick={handleCheckout}>Checkout</button>
        </div>
      )}
    </div>
  );
};

CartPage.propTypes = {
  cartItems: PropTypes.array.isRequired,
  removeFromCart: PropTypes.func.isRequired,
};

export default CartPage;

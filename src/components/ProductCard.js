import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Modal from './Modal';
import './ProductCard.scss';
import { useDisplayMode } from '../DisplayModeContext';

const ProductCard = ({ product, addToCart, toggleSelected, isSelected }) => {
  const [showModal, setShowModal] = useState(false);
  const { displayMode } = useDisplayMode();

  const closeModal = () => {
    setShowModal(false);
  };

  if (!product) {
    return null; 
  }

  return (
    <div className={`product-card ${isSelected ? 'selected' : ''} ${displayMode === 'table' ? 'table-view' : ''}`}>
      <img className="product-image" src={product.image} alt={product.name} />
      <h3 className="product-name">{product.name}</h3>
      <p className="product-price">${product.price}</p>
      <button className={`favorite-button ${isSelected ? 'selected' : ''}`} onClick={toggleSelected}>
        <span role="img" aria-label="Favorite" className="favorite-icon">
          {isSelected ? '❤️' : '🤍'}
        </span>
      </button>
      <button className="add-to-cart-button" onClick={() => setShowModal(true)}>
        Add to cart
      </button>
      {showModal && (
        <Modal
          header={`Add ${product.name} to cart`}
          closeButton={true}
          text={`Are you sure you want to add ${product.name} to your cart?`}
          actions={
            <div>
              <button className="modal-button" onClick={() => { addToCart(product); closeModal(); }}>OK</button>
              <button className="modal-button" onClick={closeModal}>Cancel</button>
            </div>
          }
          onClose={closeModal}
        />
      )}
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
  }),
  addToCart: PropTypes.func.isRequired,
  toggleSelected: PropTypes.func.isRequired,
  isSelected: PropTypes.bool.isRequired,
};

export default ProductCard;

import React from 'react';
import './Modal.scss';

const Modal = ({ header, closeButton, text, actions, onClose, modalStyle, headerStyle }) => {
  const closeModal = (event) => {
    if (event.target === event.currentTarget) {
      onClose();
    }
  };

  return (
    <div className="modal-overlay" onClick={closeModal}>
      <div className="modal" style={modalStyle}>
        <div className="modal-header" style={headerStyle}>
          <h2 style={{ color: 'white' }}>{header}</h2>
          {closeButton && <span className="modal-close" onClick={onClose}>×</span>}
        </div>
        <div className="modal-content">
          {text}
        </div>
        <div className="modal-actions">
          {actions}
        </div>
      </div>
    </div>
  );
};

export default Modal;

import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import '../App.scss'; 


const CheckoutForm = () => {
  const initialValues = {
    firstName: '',
    lastName: '',
    age: '',
    address: '',
    phoneNumber: '',
  };

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required('First Name is required'),
    lastName: Yup.string().required('Last Name is required'),
    age: Yup.number().required('Age is required').positive('Age must be a positive number'),
    address: Yup.string().required('Address is required'),
    phoneNumber: Yup.string()
      .required('Phone Number is required')
      .matches(/^\d+$/, 'Phone Number must contain only digits'),
  });

  const handleSubmit = (values, { resetForm }) => {
    console.log('Form submitted:', values);

    setTimeout(() => {
      console.log('Purchase completed:', values);
      resetForm(); 
    }, 2000);
  };

  return (
    <div className="checkout-form">
      <h3>Checkout</h3>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit} 
      >
        <Form>
          {}
          <div className="form-group">
            <label htmlFor="firstName">First Name</label>
            <Field type="text" id="firstName" name="firstName" />
            <ErrorMessage name="firstName" component="div" className="error-message" />
          </div>
          <div className="form-group">
            <label htmlFor="lastName">Last Name</label>
            <Field type="text" id="lastName" name="lastName" />
            <ErrorMessage name="lastName" component="div" className="error-message" />
          </div>
          <div className="form-group">
            <label htmlFor="age">Age</label>
            <Field type="number" id="age" name="age" />
            <ErrorMessage name="age" component="div" className="error-message" />
          </div>
          <div className="form-group">
            <label htmlFor="address">Address</label>
            <Field type="text" id="address" name="address" />
            <ErrorMessage name="address" component="div" className="error-message" />
          </div>
          <div className="form-group">
            <label htmlFor="phoneNumber">Phone Number</label>
            <Field type="text" id="phoneNumber" name="phoneNumber" />
            <ErrorMessage name="phoneNumber" component="div" className="error-message" />
          </div>
          
          {}
          <button type="submit" className="checkout-button">Checkout</button>
        </Form>
      </Formik>
    </div>
  );
};

export default CheckoutForm;
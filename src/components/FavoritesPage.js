import React from 'react';
import PropTypes from 'prop-types';
import ProductCard from './ProductCard';


const FavoritesPage = ({ products, selectedProducts, toggleSelected }) => {
  const favoriteProducts = products.filter((product) => selectedProducts.includes(product.id));

  return (
    <div className="favorites-page">
      <h2>Favorites</h2>
      <div className="product-list favorites-product-list">
        {favoriteProducts.map((product) => (
          <ProductCard
            key={product.id}
            product={product}
            toggleSelected={() => toggleSelected(product.id)}
            isSelected={selectedProducts.includes(product.id)}
          />
        ))}
      </div>
    </div>
  );
};

FavoritesPage.propTypes = {
  products: PropTypes.array.isRequired,
  selectedProducts: PropTypes.array.isRequired,
  toggleSelected: PropTypes.func.isRequired,
};

export default FavoritesPage;

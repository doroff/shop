import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import './App.scss';
import Modal from './components/Modal';
import { DisplayModeProvider } from './DisplayModeContext';
import CartPage from './components/CartPage';
import FavoritesPage from './components/FavoritesPage';
import HomePage from './HomePage';
import SmartTVImage from './assets/SmartTV.jpg';
import RefrigeratorImage from './assets/refrigerator.jpg';
import WashingMachineImage from './assets/washingmachine.jpg';
import MicrowaveImage from './assets/microwave.jpg';
import CoffeeMakerImage from './assets/coffeemaker.jpg';
import BlenderImage from './assets/blender.jpg';
import ToasterImage from './assets/toaster.jpg';
import VacuumCleanerImage from './assets/vacuumcleaner.jpg';
import AirPurifierImage from './assets/airpurifier.jpg';
import ElectricKettleImage from './assets/electrickettle.jpg';
import { Provider } from 'react-redux';
import store from './reducers/store';

function App() {
  const [showFirstModal, setShowFirstModal] = useState(false);
  const [showSecondModal, setShowSecondModal] = useState(false);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    const savedSelectedProducts = localStorage.getItem('selectedProducts');
    if (savedSelectedProducts) {
      setSelectedProducts(JSON.parse(savedSelectedProducts));
    }

    const savedCartItems = localStorage.getItem('cartItems');
    if (savedCartItems) {
      setCartItems(JSON.parse(savedCartItems));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('selectedProducts', JSON.stringify(selectedProducts));
    localStorage.setItem('cartItems', JSON.stringify(cartItems));
  }, [selectedProducts, cartItems]);
  const products = [
    {
      name: 'Smart TV',
      price: 699.99,
      image: SmartTVImage,
      id: 1,
      color: 'black',
    },
    {
      name: 'Refrigerator',
      price: 499.99,
      image: RefrigeratorImage,
      id: 2,
      color: 'white',
    },
    {
      name: 'Washing Machine',
      price: 399.99,
      image: WashingMachineImage,
      id: 3,
      color: 'gray',
    },
    {
      name: 'Microwave Oven',
      price: 129.99,
      image: MicrowaveImage,
      id: 4,
      color: 'silver',
    },
    {
      name: 'Coffee Maker',
      price: 59.99,
      image: CoffeeMakerImage,
      id: 5,
      color: 'black',
    },
    {
      name: 'Blender',
      price: 79.99,
      image: BlenderImage,
      id: 6,
      color: 'red',
    },
    {
      name: 'Toaster',
      price: 29.99,
      image: ToasterImage,
      id: 7,
      color: 'white',
    },
    {
      name: 'Vacuum Cleaner',
      price: 149.99,
      image: VacuumCleanerImage,
      id: 8,
      color: 'blue',
    },
    {
      name: 'Air Purifier',
      price: 199.99,
      image: AirPurifierImage,
      id: 9,
      color: 'white',
    },
    {
      name: 'Electric Kettle',
      price: 39.99,
      image: ElectricKettleImage,
      id: 10,
      color: 'black',
    },
  ];

  const addToCart = (product) => {
    setCartItems([...cartItems, { ...product, isInCart: true }]);
    closeFirstModal();
  };

  const removeFromCart = (productId) => {
    setCartItems(cartItems.filter((product) => product.id !== productId));
  };

  const toggleSelectedProduct = (productId) => {
    if (selectedProducts.includes(productId)) {
      setSelectedProducts(selectedProducts.filter((id) => id !== productId));
    } else {
      setSelectedProducts([...selectedProducts, productId]);
    }
  };

  const openFirstModal = () => {
    setShowFirstModal(true);
  };

  const closeFirstModal = () => {
    setShowFirstModal(false);
  };

  const openSecondModal = () => {
    setShowSecondModal(true);
  };

  const closeSecondModal = () => {
    setShowSecondModal(false);
  };

  const cartItemCount = cartItems.length;
  const selectedItemCount = selectedProducts.length;

  return (
    <Provider store={store}>
            <DisplayModeProvider>
      <Router>
      <div className="App">
        <header className="App-header">
          <nav className="menu">
            <ul>
              <li>
                <Link to="/" className="menu-link">
                  Home
                </Link>
              </li>
              <li>
                <Link to="/cart" className="menu-link">
                  Cart ({cartItemCount})
                </Link>
              </li>
              <li>
                <Link to="/favorites" className="menu-link">
                  Favorites ({selectedItemCount})
                </Link>
              </li>
            </ul>
          </nav>
          <div className="icons">
            <div className="cart-icons">
              <span className="icon" onClick={openFirstModal}>
                🛒
              </span>
              <span className="count">{cartItemCount}</span>
            </div>
            <div className="selected-icons">
              <span className="icon" onClick={openSecondModal}>
                ❤️
              </span>
              <span className="count">{selectedItemCount}</span>
            </div>
          </div>
        </header>

        <Routes>
          <Route path="/" element={<HomePage products={products} addToCart={addToCart} toggleSelected={toggleSelectedProduct} selectedProducts={selectedProducts} />} />
          <Route path="/cart" element={<CartPage cartItems={cartItems} removeFromCart={removeFromCart} />} />
          <Route path="/favorites" element={<FavoritesPage products={products} selectedProducts={selectedProducts} toggleSelected={toggleSelectedProduct} />} />
        </Routes>

        {/*  */}
      </div>
      </Router>
      </DisplayModeProvider>
    </Provider>
  );
}
function RootApp() {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
}

export default RootApp;
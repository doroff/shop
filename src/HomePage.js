import React from 'react';
import PropTypes from 'prop-types';
import ProductCard from './components/ProductCard';
import ProductsViewToggle from './ProductsViewToggle'; 
import { useDisplayMode } from './DisplayModeContext';
const HomePage = ({ products, addToCart, toggleSelected, selectedProducts }) => {
  const { displayMode } = useDisplayMode();

  return (
    <div className="product-list-container">
      <div className="product-list">
        <div className="product-row">
          <header className="product-header">
            <h1>Products</h1>
            <ProductsViewToggle />
          </header>
          {displayMode === 'cards' && (
            <div className="product-card-row">
              {products.map((product) => (
                <ProductCard
                  key={product.id}
                  product={product}
                  addToCart={() => addToCart(product)}
                  toggleSelected={() => toggleSelected(product.id)}
                  isSelected={selectedProducts.includes(product.id)}
                />
              ))}
            </div>
          )}
          {displayMode === 'table' && (
            <div className="table-view">
              <table className="product-table">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {products.map((product) => (
                    <tr key={product.id}>
                      <td>
                        <img className="product-image" src={product.image} alt={product.name} />
                      </td>
                      <td>{product.name}</td>
                      <td>${product.price}</td>
                      <td>
                        <button className="add-to-cart-button" onClick={() => addToCart(product)}>
                          Add to cart
                        </button>
                        <button className={`favorite-button ${selectedProducts.includes(product.id) ? 'selected' : ''}`} onClick={() => toggleSelected(product.id)}>
                          <span role="img" aria-label="Favorite" className="favorite-icon">
                            {selectedProducts.includes(product.id) ? '❤️' : '🤍'}
                          </span>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

HomePage.propTypes = {
  products: PropTypes.array.isRequired,
  addToCart: PropTypes.func.isRequired,
  toggleSelected: PropTypes.func.isRequired,
  selectedProducts: PropTypes.array.isRequired,
};

export default HomePage;

export const openFirstModal = () => ({ type: 'OPEN_FIRST_MODAL' });
export const closeFirstModal = () => ({ type: 'CLOSE_FIRST_MODAL' });
export const openSecondModal = () => ({ type: 'OPEN_SECOND_MODAL' });
export const closeSecondModal = () => ({ type: 'CLOSE_SECOND_MODAL' });
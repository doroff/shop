export const fetchDataSuccess = (data) => ({ type: 'FETCH_DATA_SUCCESS', payload: data });

export const fetchData = () => {
  return async (dispatch) => {
    try {
      const response = await fetch('URL_СЮДИ_ВСТАВИТИ_АДРЕСУ_API');
      const data = await response.json();

      dispatch(fetchDataSuccess(data));
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };
};
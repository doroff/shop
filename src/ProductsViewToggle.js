import React from 'react';
import { useDisplayMode } from './DisplayModeContext'; 

function ProductsViewToggle() {
  const { displayMode, setDisplayMode } = useDisplayMode();

  const handleToggle = (newMode) => {
    setDisplayMode(newMode);
  };

  return (
    <div className="view-toggle">
      <label>
        <input
          type="radio"
          value="cards"
          checked={displayMode === 'cards'}
          onChange={() => handleToggle('cards')}
        />
        Card View
      </label>
      <label>
        <input
          type="radio"
          value="table"
          checked={displayMode === 'table'}
          onChange={() => handleToggle('table')}
        />
        Table View
      </label>
    </div>
  );
}

export default ProductsViewToggle;

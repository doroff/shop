export const SET_PRODUCTS = 'SET_PRODUCTS';
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export const setProducts = (products) => ({
  type: SET_PRODUCTS,
  payload: products,
});

export const openModal = (modalName) => ({
  type: OPEN_MODAL,
  payload: modalName,
});

export const closeModal = () => ({
  type: CLOSE_MODAL,
});

export const fetchProducts = () => async (dispatch) => {
    try {
      const response = await fetch('YOUR_API_URL');
      const data = await response.json();
      dispatch(setProducts(data));
    } catch (error) {
      console.error('Error fetching products:', error);
    }
  };

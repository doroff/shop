import { combineReducers } from 'redux';
import { SET_PRODUCTS, OPEN_MODAL, CLOSE_MODAL } from './actions';

const productsReducer = (state = [], action) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return action.payload;
    default:
      return state;
  }
};

const modalsReducer = (state = { firstModal: false, secondModal: false }, action) => {
  switch (action.type) {
    case OPEN_MODAL:
      return { ...state, [action.payload]: true };
    case CLOSE_MODAL:
      return { ...state, firstModal: false, secondModal: false };
    default:
      return state;
  }
};

export default combineReducers({
  products: productsReducer,
  modals: modalsReducer,
});
